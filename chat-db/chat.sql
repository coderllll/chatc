/*
 Navicat MySQL Data Transfer

 Source Server         : 8.0
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : chat

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 04/02/2024 21:30:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `send_id` bigint(20) NOT NULL COMMENT '发送人id',
  `recipient_id` bigint(20) NOT NULL COMMENT '接收人id',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '聊天内容',
  `type` int(11) NOT NULL DEFAULT 0 COMMENT '内容类型：0html文本，1图片, 2视频',
  `status` int(11) NOT NULL COMMENT '是否发送成功：0否，1是',
  `timestamp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '时间戳',
  `create_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建时间',
  `is_read` tinyint(1) NOT NULL COMMENT '是否阅读：0否，1是',
  `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES (31, 2, 3, 'test', 4, 1, '1706789422156', '2024-02-01 20:10:22', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (32, 2, 1, 'test', 4, 1, '1706789422156', '2024-02-01 20:10:22', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (33, 1, 3, '<p>?</p>', 0, 1, '1706789666057', '2024-02-01 20:14:26', 1, '/avatar/1.jpg');
INSERT INTO `chat` VALUES (34, 3, 2, '<p><img src=\"/emoji/酷笑.gif\" class=\"emo-image\"/><br/></p>', 0, 1, '1706790541442', '2024-02-01 20:29:01', 1, '/avatar/2.jpg');
INSERT INTO `chat` VALUES (35, 2, 3, '', 4, 1, '1706790604532', '2024-02-01 20:30:04', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (36, 2, 1, '', 4, 1, '1706790604532', '2024-02-01 20:30:04', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (37, 2, 1, 'asdfsdaf', 4, 1, '1706790622582', '2024-02-01 20:30:22', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (38, 2, 3, 'asdfsdaf', 4, 1, '1706790622582', '2024-02-01 20:30:22', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (39, 3, 2, '<p>ddd</p>', 0, 1, '1706791688153', '2024-02-01 20:48:08', 1, '/avatar/2.jpg');
INSERT INTO `chat` VALUES (40, 3, 2, '<p>ddd</p>', 0, 1, '1706791721698', '2024-02-01 20:48:41', 1, '/avatar/2.jpg');
INSERT INTO `chat` VALUES (41, 2, 3, '<p>hello</p>', 0, 1, '1706795367889', '2024-02-01 21:49:27', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (42, 2, 1, 'ddd', 4, 1, '1706801976095', '2024-02-01 23:39:36', 0, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (43, 2, 3, 'ddd', 4, 1, '1706801976095', '2024-02-01 23:39:36', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (44, 2, 3, 'ddd', 4, 1, '1706802042150', '2024-02-01 23:40:42', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (45, 2, 1, 'ddd', 4, 1, '1706802042150', '2024-02-01 23:40:42', 0, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (46, 3, 1, '<p>dsfsdd</p>', 0, 1, '1706802082137', '2024-02-01 23:41:22', 0, '/avatar/2.jpg');
INSERT INTO `chat` VALUES (47, 2, 3, 'ddd', 4, 1, '1706802085584', '2024-02-01 23:41:25', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (48, 2, 1, 'ddd', 4, 1, '1706802085584', '2024-02-01 23:41:25', 0, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (49, 2, 3, '<p>111</p>', 0, 1, '1706802327649', '2024-02-01 23:45:27', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (50, 2, 3, '<p>111</p>', 0, 1, '1706802328525', '2024-02-01 23:45:28', 1, '/avatar/4.jpg');
INSERT INTO `chat` VALUES (51, 2, 1, '<p>dd</p>', 0, 1, '1706802710339', '2024-02-01 23:51:50', 0, '/avatar/4.jpg');

-- ----------------------------
-- Table structure for group_chat
-- ----------------------------
DROP TABLE IF EXISTS `group_chat`;
CREATE TABLE `group_chat`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `send_id` bigint(20) NOT NULL,
  `group_chat_id` bigint(20) NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `timestamp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_at` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `send_id`(`send_id`) USING BTREE,
  INDEX `group_chat_id`(`group_chat_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_chat
-- ----------------------------

-- ----------------------------
-- Table structure for group_chat_list
-- ----------------------------
DROP TABLE IF EXISTS `group_chat_list`;
CREATE TABLE `group_chat_list`  (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `groupId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_leader` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 86 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_chat_list
-- ----------------------------
INSERT INTO `group_chat_list` VALUES (85, '1706802085584', 'ddd', '2');
INSERT INTO `group_chat_list` VALUES (84, '1706802042150', 'ddd', '2');
INSERT INTO `group_chat_list` VALUES (83, '1706801976095', 'ddd', '2');
INSERT INTO `group_chat_list` VALUES (82, '1706791688153', '<p>ddd</p>', '3');
INSERT INTO `group_chat_list` VALUES (81, '1706790622582', 'asdfsdaf', '2');
INSERT INTO `group_chat_list` VALUES (80, '1706790604532', '', '2');
INSERT INTO `group_chat_list` VALUES (79, '1706790541442', '<p><img src=\"/emoji/酷笑.gif\" class=\"emo-image\"/><br/></p>', '3');
INSERT INTO `group_chat_list` VALUES (78, '1706789666057', '<p>?</p>', '1');
INSERT INTO `group_chat_list` VALUES (77, '1706789422156', 'test', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id，自增',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `online_status` tinyint(4) NULL DEFAULT NULL COMMENT '在线状态',
  `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `session_history` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '历史会话',
  `chat_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'socketId',
  `timestamp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '时间戳',
  `friends` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '好友列表',
  `group_chat_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '群聊列表',
  PRIMARY KEY (`id`, `nick_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'cacb9f21656613953063', 'cacb9f21656613953063', '66ce2829f028ecd01cf1553172e347c5', NULL, 0, '/avatar/1.jpg', '456@lingshulian.com', '[{\"id\":3,\"name\":\"5b0b4901656618173483\",\"nick_name\":\"5b0b4901656618173483\",\"mobile\":null,\"online_status\":0,\"avatar\":\"/avatar/2.jpg\",\"email\":\"789@lingshulian.com\",\"session_history\":\"[{\\\"id\\\":2,\\\"name\\\":\\\"d869e5b1656613910852\\\",\\\"nick_name\\\":\\\"d869e5b1656613910852\\\",\\\"mobile\\\":null,\\\"online_status\\\":0,\\\"avatar\\\":\\\"/avatar/4.jpg\\\",\\\"email\\\":\\\"123@lingshulian.com\\\",\\\"session_history\\\":null,\\\"chat_id\\\":\\\"vbtvKLOAKve-vjslAAAj\\\",\\\"timestamp\\\":1706607888052,\\\"iat\\\":1706607888,\\\"exp\\\":1706636688,\\\"historySessionList\\\":\\\"\\\"}]\",\"chat_id\":\"gagcrAKqJuCs4apJAAAD\",\"timestamp\":1706612758560,\"iat\":1706612758,\"exp\":1706641558,\"historySessionList\":\"\"},{\"id\":2,\"name\":\"d869e5b1656613910852\",\"password\":\"66ce2829f028ecd01cf1553172e347c5\",\"mobile\":null,\"avatar\":\"/avatar/4.jpg\",\"email\":\"123@lingshulian.com\",\"timestamp\":\"1706789409223\",\"friends\":null,\"nickName\":\"d869e5b1656613910852\",\"onlineStatus\":1,\"chatId\":\"Zdn2U5xfnSbv4jeVAAAb\",\"groupChatList\":\"1706789422156,\"}]', 'DLBFOrFRLxqlN1BJAAAJ', '1706790591457', NULL, '1706789422156,1706789666057,');
INSERT INTO `user` VALUES (2, 'd869e5b1656613910852', 'd869e5b1656613910852', '66ce2829f028ecd01cf1553172e347c5', NULL, 0, '/avatar/4.jpg', '123@lingshulian.com', '[{\"id\":3,\"name\":\"5b0b4901656618173483\",\"password\":\"66ce2829f028ecd01cf1553172e347c5\",\"mobile\":null,\"avatar\":\"/avatar/2.jpg\",\"email\":\"789@lingshulian.com\",\"timestamp\":\"1656618329382\",\"nickName\":\"5b0b4901656618173483\",\"onlineStatus\":0,\"chatId\":\"ft_ihrYlvGND6K4cAAAf\"},{\"id\":1,\"name\":\"cacb9f21656613953063\",\"password\":\"66ce2829f028ecd01cf1553172e347c5\",\"mobile\":null,\"avatar\":\"/avatar/1.jpg\",\"email\":\"456@lingshulian.com\",\"timestamp\":\"1706607832053\",\"nickName\":\"cacb9f21656613953063\",\"onlineStatus\":0,\"chatId\":\"rG-Aa6LrWvHqLwUjAAAz\"}]', '6DTnV-a1cAfpudSoAAAH', '1706803043973', NULL, '1706789422156,1706790604532,1706790622582,1706801976095,1706802042150,1706802085584,');
INSERT INTO `user` VALUES (3, '5b0b4901656618173483', '5b0b4901656618173483', '66ce2829f028ecd01cf1553172e347c5', NULL, 0, '/avatar/2.jpg', '789@lingshulian.com', '[{\"id\":2,\"name\":\"d869e5b1656613910852\",\"nick_name\":\"d869e5b1656613910852\",\"mobile\":null,\"online_status\":0,\"avatar\":\"/avatar/4.jpg\",\"email\":\"123@lingshulian.com\",\"session_history\":null,\"chat_id\":\"vbtvKLOAKve-vjslAAAj\",\"timestamp\":1706607888052,\"iat\":1706607888,\"exp\":1706636688,\"historySessionList\":\"\"},{\"id\":1,\"name\":\"cacb9f21656613953063\",\"password\":\"66ce2829f028ecd01cf1553172e347c5\",\"mobile\":null,\"avatar\":\"/avatar/1.jpg\",\"email\":\"456@lingshulian.com\",\"timestamp\":\"1706607832053\",\"nickName\":\"cacb9f21656613953063\",\"onlineStatus\":0,\"chatId\":\"rG-Aa6LrWvHqLwUjAAAz\"}]', 'qZucG2LUbWXLNB-_AAAF', '1706801968556', NULL, '1706789422156,1706790541442,1706791688153,');

SET FOREIGN_KEY_CHECKS = 1;
