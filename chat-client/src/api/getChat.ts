import request from "@/utils/request"

// 搜索用户信息
export function getUserInfos(data: any) {
    return request({
        url: "/api/getUserInfos",
        method: "post",
        data
    })
}