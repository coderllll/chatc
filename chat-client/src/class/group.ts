import User from './User';
class Group {
    id: string;
    groupName: string;
    groupLeader: string;
    constructor(id: string, group_name: string, group_leader: string) {
        this.id = id,
            this.groupName = group_name,
            this.groupLeader = group_leader
    }
}

class Friends {
    id: number;
    name: string;
    onlineStatus: boolean;
    mobile: string;
    nickName: string;
    avatar: string;
    isTrue: string | boolean | number | undefined
    constructor(id: number, name: string, onlineStatus: boolean, Code: string, mobile: string, nickName: string, avatar: string, isTrue: boolean | undefined) {
        this.id = id;
        this.name = name;
        this.onlineStatus = onlineStatus;
        this.mobile = mobile;
        this.nickName = nickName;
        this.avatar = avatar;
        isTrue = isTrue
    }
}

class GroupConversition {
    sendId: number;
    recipientId: number | [User];
    content: string;    //0html文本，1图片, 2视频，3群聊，4邀请
    type: number;
    status: number;
    timestamp: string | number;
    createAt: string;
    isRead: boolean;
    avatar: string;
    constructor(
        sendId: number,
        recipientId: number,
        content: string,
        type: number,
        status: number,
        timestamp: string,
        createAt: string,
        isRead: boolean,
        Avatar: string) {
        this.sendId = sendId;
        this.recipientId = recipientId;
        this.content = content;
        this.type = type;
        this.status = status;
        this.timestamp = timestamp;
        this.createAt = createAt;
        this.isRead = isRead;
        this.avatar = Avatar;
    }
}

export {
    Group,
    Friends,
    GroupConversition
}