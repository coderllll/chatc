const express = require("express");
const router = express.Router();

const { getUserInfos } = require("../model/group");

// token获取用户信息
router.post("/getUserInfos", getUserInfos);

module.exports = router;
