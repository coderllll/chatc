const userRouter = require("./user");
const emailRouter = require("./email");
const fileRouter = require("./file");
const groupRouter = require("./gruop");

module.exports = [userRouter, emailRouter, fileRouter, groupRouter];
